create database BuySaleBase

use BuySaleBase

--客户表、部门表、员工表、仓库表、出库表、退货表、库存表、商品表、商品分类表,供应商表


--客户表：客户Id ，客户名称，客户地址，客户手机，客户性别
--部门表: 部门Id，部门名称，简述
--员工表：员工Id，员工所属部门，员工名字,性别，住址，电话，出生日期，职务
--仓库表：入库商品Id，仓库名，入库时间，入库商数量，商品类别,入库地点，库存各商品数量,入库商品所属部门,负责员工Id,提供商品供应商名称
--出库表：出库商品Id，出库数量，出库仓库名，出库单号，出库时间，出库类别,出库地点,出库部门,负责员工Id
--退货表：退货Id, 退货数量，退货商品Id，退货地点，退货客户Id，退货时间，退货价格,退货原因，退货负责员工Id
--库存表: 库存I0d，商品Id，库存数量 ,库存商品所在部门，所在仓库，此种商品入库时间，此种商品出库时间
--商品表：商品Id、商品名称、商品类别Id，价格,商品、供应商名称
--商品分类表：商品分类Id，商品分类名称
--供应商表：供应商Id，供应商名称，


--客户表：客户Id ，客户名称，客户地址，客户手机，客户性别
create table Users 
(
	Users_id int primary key identity(1,1) not null,
	Users_name nvarchar(10) not null,
	Users_address nvarchar(20) not null,
	Users_phone varchar(11) not null,
	USers_sex varchar(2) not null
)


--部门表:部门Id，部门名称，简述
create table Srction 
(	Srction_id int primary key identity(1,1) not null,
	Srction_name nvarchar(10) not null,
	Srction_description nvarchar(200) not null

)

--员工表：员工Id，员工所属部门，员工名字,性别，住址，电话，出生日期，职务
create table Employee
(
	Employee_id int primary key identity(1,1) not null,
	Employee_srction varchar(10) not null,
	Employee_name nvarchar(10) not null,
	Employee_sex varchar(2) not null,
	Employee_address varchar(20) not null,
	Employee_phone varchar(11) not null,
	Employee_birth varchar(10) not null,
	Employee_job varchar(10) not null

)


--仓库表：入库商品Id，仓库名，入库时间，入库商数量，商品类别,入库地点，库存各商品数量,入库商品所属部门,负责员工Id,提供商品供应商名称
create table InStorage
(
	InCommodity_id int primary key identity(1,1) not null,
	Storage_name nvarchar(10) not null, 
	InStorage_time smalldatetime not null,
	InStorage_sum int not null,
	InCommodity_class nvarchar(10) not null ,
	InCommodity_location nvarchar(30) not null,
	InCommodity_allSum varchar(20) not null,
	InCommodity_srction varchar(20) not null,
	Employee_id int not null,
	Factory_name nvarchar(20) not null
)


--出库表：出库商品Id，出库商品数量，出库仓库名，出库单号，出库时间，出库商品类别,出库地点,出库部门,负责员工Id
create table OutStorage
(
	OutCommodity_id int primary key identity(1,1) not null,
	OutCommodity_sum int not null,
	Storage_name nvarchar(10) not null,
	OutStorage_time smalldatetime not null,
	OutCommodity_number varchar(15) not null,
	OutCommodity_class nvarchar(10) not null, 
	OutCommodity_address nvarchar(30) not null,
	OutCommodity_srction nvarchar(10) not null,
	Employee_id int not null
)

--退货表：退货Id, 退货数量，退货商品Id，退货地点，退货客户Id ，退货时间，退货价格,退货原因，退货负责员工Id
create table TReturns
(
	TReturns_id int primary key identity(1,1) not null,
	TReturns_sum int not null,
	Commodity_id int not null,
	TReturns_address nvarchar(20) not null,
	Users_id int not null,
	TReturns_time smalldatetime not null,
	TReturns_price float not null,
	TReturns_cause nvarchar(200) not null,
	Employee_id int not null,		

)

--库存表:库存Id，商品Id,库存数量 库存商品所在部门，所在仓库，此种商品入库时间，此种商品出库时间，
create table Inventory 
( 
	Inventory_id int primary key identity(1,1) not null,
	Commodity_id int not null,
	Invertory_sum int not null,
	Invertory_srction nvarchar(10) not null,
	Invertory_storage nvarchar(10) not null,
	Invertory_inTime smalldatetime not null,
	Invertory_outTime smalldatetime not null
	
)


--商品表：商品Id、商品名称、商品类别Id，价格,商品供应商名称
create table Commodity
(
	Commodity_id int primary key identity(1,1) not null,
	Commodity_name nvarchar(10) not null,
	Commodity_classId nvarchar(10) not null,
	Price float not null,
	Factory_name nvarchar(20) not null
)

--商品分类表：商品分类Id，商品分类名称
create table Commodity_class
(
	Commodity_id int primary key identity(1,1) not null,
	Commodity_name nvarchar(10) not null 

)


--供应商表：供应商Id，供应商名称
create table Factory
(
	Factory_id int primary key identity(1,1) not null,
	Factory_name nvarchar(20) not null
)