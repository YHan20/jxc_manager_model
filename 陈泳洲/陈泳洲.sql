create database StockSellStorage
use StockSellStorage

create table Supplier(--供应商表
 supplier_id int primary key  identity(1,1),	--供应商id
 supplier_name varchar(255),	--供应商名字
 supplier_site varchar(255),	--供应商地址
 supplier_phone varchar(255),	--供应商电话
 supplier_contact varchar(255),	--供应商联系人
 supplier_del int check(supplier_del=0 or supplier_del=1) default 0  --逻辑删除
)


create table Client(--客户表
client_id int primary key  identity(1,1),--客户id
client_name  varchar(255),	--客户名称
client_site varchar  (255),	--客户地址
client_phone varchar(255),	--客户电话
client_del int check(Client_del=0 or Client_del=1) default 0  --逻辑删除
)

create table warehouse (--仓库表
warehouse_id int primary key  identity(1,1),--仓库id
commodity_id  int,--商品id
warehouse_quantity int ,--商品数量 
commodity_price money,--参考价格
warehouse_del int check(warehouse_del=0 or warehouse_del=1) default 0  --逻辑删除
)



create table commodity(--商品表
commodity_id int ,--商品id
commodity_price money,--参考价格
commodity_del int check(commodity_del=0 or commodity_del=1) default 0  --逻辑删除
)
create table stock (-- 进货表
stock_id int ,--进货id
commodity_id  int,--商品id
stock_quantity int ,--进货数量
stock_price money,--进货价格
stock_del int check(stock_del=0 or stock_del=1) default 0--逻辑删除 
)


create table InWarehouse(--入库表
InWarehouse_id int primary key  identity(1,1),--入库id
warehouse_id int,--仓库id
commodity_id  int,--商品id
warehouse_quantity int ,--商品数量
commodity_price money,--参考价格
InWarehouse_Time  date,--入库时间
InWarehouse_del int check(InWarehouse_del=0 or InWarehouse_del=1) default 0  --逻辑删除
)


create table OotWarehouse(--出库表
OotWarehouse_id int,--出库id
warehouse_id int,--仓库id
commodity_id  int,--商品id
warehouse_quantity int ,--商品数量
OutWarehouse_Time  date,--入库时间
OotWarehouse_del int check(OotWarehouse_del=0 or OotWarehouse_del=1) default 0  --逻辑删除
)


create table Market(--销售表
Market_id int primary key  identity(1,1),-- 销售id
commodity_id  int,--商品id
warehouse_quantity int ,--商品数量
commodity_price money,--参考价格
Market_Time date, --销售时间
client_id int,--客户id
Market_del int check(Market_del=0 or Market_del=1) default 0  --逻辑删除
)

create table SalesReturn(--退货表
SalesReturn_id int primary key  identity(1,1),-- 退货id
Market_id int , --销售表id
SalesReturn_price money,--退货金额
SalesReturn_warehouse_id int,--退入仓库
SalesReturn_Time date,--退货时间
client_id int,--客户id
salesReturn_cause varchar(1000) ,--退货原因
SalesReturn_del int check(SalesReturn_del=0 or SalesReturn_del=1) default 0  --逻辑删除

)